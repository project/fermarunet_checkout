
class Ajax extends Drupal.Ajax
{
	constructor(...args) {super(...args);}

	_fn = null;

	setSuccess(fn) {this._fn = fn;}
	success(response, status){ if(this._fn) this._fn(response, status);}
};

//##########################################################################

function Fiscalization(idOrder)
{
	var ajax = new Ajax(false, false, {url: '/admin/commerce/fiscalization/fiscalization/'+idOrder});
	console.log(ajax);
	//ajax.eventResponse(ajax, {});

	ajax.setSuccess(function (response, status)
	{
		var aReturn = response;
		alert(aReturn.data);

		if(aReturn.success)
			location.reload();
	}
	);

	ajax.execute();
}
