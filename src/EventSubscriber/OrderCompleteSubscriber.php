<?php

namespace Drupal\fermarunet_checkout\EventSubscriber;

use Drupal\fermarunet_checkout\CPangaea;
use Drupal\fermarunet_checkout\func;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Drupal\Core\Entity\EntityTypeManager;

//##########################################################################

class OrderCompleteSubscriber implements EventSubscriberInterface
{
  public function __construct() 
	{

  }

  //************************************************************************

  static function getSubscribedEvents() 
	{
    //сообщение перед помещением заказа (для расформирования позиций заказа, если есть маркированные товары с количеством более 1 то надо сделать их по 1 чтобы для каждого указывать код маркировки)
    $events['commerce_order.place.pre_transition'] = ['orderCompleteHandlerPre'];

    //сообщение после сохранения оплаты по заказу, для фискализации если нет маркировки в товарах
    $events['commerce_payment.commerce_payment.presave'] = ['orderCompleteHandlerPost'];
		//$events['commerce_order.fulfill.post_transition'] = ['orderCompleteHandlerPost'];
    return $events;
  }

  //************************************************************************

  public function orderCompleteHandlerPre(WorkflowTransitionEvent $event) 
  {
    //разделение позиций в заказе по одному
    $order = $event->getEntity();
    $hasRefresh = false;
    $aItems = $order->getItems();
    foreach($aItems as $oItem)
    {
      $oProduct = $oItem->getPurchasedEntity()->getProduct();
      $aProduct = $oProduct->toArray();
      $isMarked = (array_key_exists("marked", $aProduct) ? $aProduct["marked"][0]["value"] : false);
      $iCount = intval($oItem->getQuantity());

      if($iCount > 1 && $isMarked)
      {
        $hasRefresh = true;
        $order->removeItem($oItem);
        $oItem->setQuantity(1);
        for($i=0; $i<$iCount; ++$i)
        {
          $oItemCopy = $oItem->createDuplicate();
          $oItemCopy->save();
          $order->addItem($oItemCopy);
        }
        $oItem->delete();
      }
    }
    
    if($hasRefresh)
    {
      $order->setRefreshState(\Drupal\commerce_order\Entity\OrderInterface::REFRESH_ON_SAVE);
    }
  }

  //************************************************************************

  public function orderCompleteHandlerPost(\Drupal\commerce_payment\Event\PaymentEvent $event) 
	{
    //\Drupal\commerce_order\Entity\OrderInterface $order
    $oPayment = $event->getPayment();
    $order = $oPayment->getOrder();
    $idOrder = $order->id();

    //если оплата не проведена тогда ничего не делаем
    if(!$oPayment->isCompleted())
      return;

    $aUserData = func::getUserData($order);

    //\Drupal\commerce_order\Entity\OrderItemInterface[]
    $aItems = $order->getItems();
    $aItems2 = [];
    $existsMarked = false;

    //формирование списка товаров, похоже на #func::getOrderData [items]
    foreach($aItems as $oItem)
    {
      $oOrderItem = $oItem;
      
      if(!$existsMarked)
      {
        $oProduct = $oItem->getPurchasedEntity()->getProduct();
        $aProduct = $oProduct->toArray();
        if(array_key_exists("marked", $aProduct))
          $existsMarked = boolval($aProduct["marked"][0]["value"]);
      }

      $aItem = $oItem->toArray();
      $aAdjustments = $aItem["adjustments"];
      $fTax = 0;

      foreach($aAdjustments as $aAdjustment)
      {
        $oAdjustment = $aAdjustment["value"];
        if($oAdjustment->getType() == 'tax')
        {
          $fTax = $oAdjustment->getPercentage()*100;
          break;
        }
      }

      $aItems2[] = [
        "name" => $oItem->getTitle(),
        "count" => intval($oItem->getQuantity()),
        "price" => $oItem->getUnitPrice()->getNumber(),
        "vat" => $fTax
      ];
    }

    //если есть маркированные товары, тогда завершаем
    if($existsMarked)
      return;

    $aContent = func::getContent($aUserData, $aItems2, true);

    $aModuleSettings = func::getSettings();
		$sUUID = CPangaea::getUUID();
		$oPangaea = new CPangaea($aModuleSettings["tin"], $aModuleSettings["token"]);
    $aResponse = $oPangaea->receipt($aModuleSettings["store"], $aContent, $sUUID);
    
    $sMessage = "Заказ №$idOrder: Поставлено в очередь";

    //если чек не пробился и непоставлен в очередь - не записываем в БД, какая-то ошибка, наверх прокинуть не вариант
		if($aResponse["code"] == 202 && $aResponse["code"] == 201)
			return;

    $iStatus = ($aResponse["code"] == 201 ? 1 : 0);
    
    $query = \Drupal::database()->insert('fermarunet_checkout_tabs');
    $query->fields([
      'order' => $idOrder,
      'status' => $iStatus,
      'uuid' => $sUUID,
      'content' => json_encode($aContent, JSON_UNESCAPED_UNICODE),
      'response_txt' => $aResponse["response"],
      'response_code' => $aResponse["code"],
      "timestamp" => date("Y-m-d H:i:s"),
      'count' => 1,
    ]);
    $query->execute();
	}
}
