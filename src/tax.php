<?php

namespace Drupal\fermarunet_checkout;

//##########################################################################

class tax
{
	//! Ставка НДС (% => значение для кассы)
	public static $m_aTaxes = [
		"20" => 1,	// 20%
		"10" => 2,	// 10%
		"0" => 5,		// 0%
	];

	//########################################################################

	//! возвращает значение ставки для кассы по проценту
	static public function getValueByPercent($iPercent)
	{
		$iPercent = strval($iPercent);

		return (array_key_exists($iPercent, self::$m_aTaxes) ? self::$m_aTaxes[$iPercent] : 5);
	}

	//! возвращает процент по значению для кассы
	static public function getPercentByValue($iValue)
	{
		$iValue = intval($iValue);

		return (in_array($iValue, self::$m_aTaxes) ? array_search($iValue, self::$m_aTaxes) : 0);
	}
};