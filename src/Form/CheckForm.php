<?php

namespace Drupal\fermarunet_checkout\Form;

use Drupal\fermarunet_checkout\CPangaea;
use Drupal\fermarunet_checkout\func;
use Drupal\fermarunet_checkout\tax;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

//##########################################################################

class CheckForm extends FormBase
{
	public function getFormId() 
	{
    return 'fermarunet_checkout_check_form';
	}
	
	public function buildForm(array $form, FormStateInterface $form_state)
	{
		$aValues = $form_state->getValues();
		$idOrder = $_GET["order"];
		$iStatus = $_GET["status"];
		$sPathModule = dirname(dirname(dirname(__FILE__)));
		$sCheck = file_get_contents("$sPathModule/static/check.html");
		$sProductOrigin = file_get_contents("$sPathModule/static/check_table_products_tr.html");

		$sSQL = "SELECT * FROM `fermarunet_checkout_tabs` WHERE `status`=$iStatus AND `order`=$idOrder";
		$sQuery = \Drupal::database()->query($sSQL);
		$oResult = $sQuery->fetch();
		$aResult = get_object_vars($oResult);

		$aCheckData = json_decode($aResult["content"], true);
		$aCheckRes = json_decode($aResult["response_txt"], true);


		$fSumWithoutVat = 0;
		$iNumber = 0;
		$aItems = [];
		foreach($aCheckData["t1059"] as $aItem)
		{
			$fSum = $aItem["t1079"]/100 * $aItem["t1023"];
			$aItems[] = [
				"number" => ++$iNumber,
				"name" => $aItem["t1030"].(array_key_exists("t1162", $aItem) || array_key_exists("marking", $aItem) ? " [M]" : ""),
				"price" => $aItem["t1079"]/100,
				"count" => $aItem["t1023"],
				"sum" => $fSum
			];

			$iVat = intval(tax::getPercentByValue($aItem["t1199"]));
			if($iVat == 0)
				$fSumWithoutVat += $fSum;
			else
				$fSumWithoutVat += $fSum - ($fSum * $iVat / (100+$iVat));
		}

		$fSumWithoutVat = round($fSumWithoutVat, 2);

		$aProducts = [];
		foreach($aItems as $aItem)
		{
			$sProduct = $sProductOrigin;
			foreach($aItem as $sKey => $sValue)
				$sProduct = str_replace("##$sKey##", $sValue, $sProduct);
			$aProducts[] = $sProduct;
		}

		$aModuleSettings = func::getSettings();
		$oPangaea = new CPangaea($aModuleSettings["tin"], $aModuleSettings["token"]);

		$aRes = $oPangaea->company();

		$sSite = ((!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off') ? 'https' : 'http') . '://' . $_SERVER['SERVER_NAME'];
		$sQR = 't=' . str_replace(' ', 'T', date('Ymd Hi', strtotime($aCheckRes['reg_time']))) .
			'&s=' . ($aCheckData['t1081'] / 100) .
			'&fn=' . $aCheckRes['fn'] .
			'&i=' . $aCheckRes['document_num'] .
			'&fp=' . $aCheckRes['fp'] .
			'&n=' . '1';

		$aCheckDataReplace = [
			"check_type" => ($aResult["status"] == 1 ? "чек / приход" : "возврат прихода"),
			"tin" => $aModuleSettings["tin"],
			"taxation" => CPangaea::getTaxationText($aRes["data"]["taxation_system"]),
			"cash" => "0",
			"non_cash" => $aCheckRes["total_amount"]/100,
			"sum_non_tax" => $fSumWithoutVat, 
			"total_sum" => $aCheckRes["total_amount"]/100,
			"link_store" => $sSite,
			"products" => implode("\n", $aProducts),
			"qr" => $sQR
		];
		$aCheckDataReplace = array_merge($aCheckDataReplace, $aCheckRes);

		foreach($aCheckDataReplace as $sKey => $sValue)
			$sCheck = str_replace("##$sKey##", $sValue, $sCheck);

		$form["check"] = ['#markup' => $sCheck];

		return $form;
	}

	public function validateForm(array &$form, FormStateInterface $form_state) {}

	public function submitForm(array &$form, FormStateInterface $form_state) {}
}
