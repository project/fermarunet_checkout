<?php

namespace Drupal\fermarunet_checkout\Controller;

use Drupal\fermarunet_checkout\func;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Controller\ControllerBase;

//##########################################################################

class AjaxController extends ControllerBase
{
  /*! фискализация заказа 
    @param idOrder ид заказа
    @return [success => true/false, data => сообщение]
  */
  public function fiscalization($idOrder) 
	{
    $aResult = ["success" => true, "data" => ""];

    $oOrder = \Drupal\commerce_order\Entity\Order::load($idOrder);
    $aUserData = func::getUserData($oOrder);

    $aOrderInfo = func::getOrderData($oOrder);
    
    if($aOrderInfo["exists_marked"])
    {
      foreach($aOrderInfo["items"] as $aItem)
      {
        if($aItem["marked"] && strlen($aItem["mark"]) == 0)
        {
          $aResult["success"] = false;
          $aResult["data"] = "У товара ".$aItem["name"]." не указан код маркировки";
        }
      }
    }

    //если ошибок не возникло - отправка
    if($aResult["success"])
    {
      $aReturn = func::sendToCheckout($oOrder->id(), $aUserData, $aOrderInfo["items"]);
      $aResult["success"] = $aReturn["success"];
      $aResult["data"] = $aReturn["msg"];
    }

    $oResponse = new AjaxResponse();
		$oResponse->setData($aResult);
    return $oResponse;
  }
}
