<?php

namespace Drupal\fermarunet_checkout;

use Drupal\fermarunet_checkout\CPangaea;
use Drupal\fermarunet_checkout\tax;

//##########################################################################

class func
{
	/*! возвращает ассоциативный массив с настройками модуля: 
		module - id модуля
		store - id магазина в онлайн кассе
		tin - инн компании
		token - токен компании
	*/
	static public function getSettings()
	{
		static $aSettings = [];

		if(count($aSettings) == 0)
		{
			$oConfig = \Drupal::config('fermarunet_checkout.settings');
			$idStore = $oConfig->get('store_id');
			$sCompanyTin = $oConfig->get('company_tin');
			$sCompanyToken = $oConfig->get('company_token');

			$aSettings["store"] = $idStore;
			$aSettings["tin"] = $sCompanyTin;
			$aSettings["token"] = $sCompanyToken;
		}

		return $aSettings;
	}

	//**************************************************************************

	//! возвращает заполнены ли настройки модуля
	static public function existsSettings()
	{
		$aSettings = GetSettings();

		return (
			strlen($aSettings["store"]) > 0 && 
			strlen($aSettings["tin"]) > 0 && 
			strlen($aSettings["token"]) > 0
		);
	}

	//##########################################################################

	/*! сборка данных для онлайн кассы
		@param aUserData #getUserData
		@param aItems #getOrderData [items]
		@param isSale true - продажа, false - возврат
		@return ассоциативный массив готовый для отправки в онлайн кассу
	*/
	static public function getContent($aUserData, $aItems, $isSale)
	{
		$aSettings = self::getSettings();

		$aContent = [
			"t1054" => ($isSale ? 1 : 0),
			"t1059" => [],
			"t1008" => (strlen($aUserData["email"]) > 0 ? $aUserData["email"] : $aUserData["phone"]),
			"t1227" => $aUserData["fio"],
			"t1187" => ((!empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off') ? 'https' : 'http') . '://' . $_SERVER['SERVER_NAME'],
		];

		//Сумма электронными (в копейках)
		$s1081 = 0;

		foreach($aItems as $aItem)
		{
			$aItem2 = [
				"t1030" => $aItem["name"],
				"t1079" => $aItem["price"]*100,
				"t1023" => $aItem["count"],
				"t1214" => 4,
				"t1212" => 1,
				"t1199" => tax::getValueByPercent($aItem["vat"]),
			];

			$s1081 += $aItem2["t1079"]*$aItem2["t1023"];

			if(strlen(trim($aItem["mark"])) > 1)
				$aItem2["marking"] = $aItem["mark"];

			//exit_print_r($aItem2);
			$aContent["t1059"][] = $aItem2;
		}

		$aContent["t1081"] = $s1081;

		return $aContent;
	}

	//**************************************************************************

	/*! получение данных клиента
		@param oOrder объект заказа
		@return [email => string, fio => string]
	*/
	static public function getUserData($oOrder)
	{
		$aProfiles = $oOrder->collectProfiles();
		$oProfileBilling = $oOrder->getBillingProfile();
		$aAddress = array_shift($oProfileBilling->toArray()["address"]);

		$aUserData = [
			"email" => (strlen($oOrder->getEmail()) > 0 ? $oOrder->getEmail() : $oOrder->getCustomer()->getInitialEmail()),
			"fio" => $aAddress["family_name"]." ".$aAddress["given_name"],
		];
		
		return $aUserData;
	}

	//**************************************************************************

	/*! получить информацию о заказе
		@param oOrder объект заказа
		@return [
			exists_marked => есть ли хотя бы один маркированный товар, 
			items => [
				name => название,
				count => количество,
				price => цена в рублях,
				vat => процент налогов [0, 100],
				marked => маркирован ли,
				mark => код маркировки если маркирован
			]
		]
	*/
	static public function getOrderData($oOrder)
	{
		//\Drupal\commerce_order\Entity\OrderItemInterface[]
		$aItems = $oOrder->getItems();
		$aItems2 = [];

		//маркирован хотя бы один товар
		$existsMarked = false;

		foreach($aItems as $oItem)
		{
			//маркирован ли текущий товар
			$existsMarkedCurr = false;
			
			$oProduct = $oItem->getPurchasedEntity()->getProduct();
			$aProduct = $oProduct->toArray();
			
			//если товар маркирован - достаем код маркировки
			if(array_key_exists("marked", $aProduct) && count($aProduct["marked"]) > 0)
			{
				$existsMarkedCurr = boolval($aProduct["marked"][0]["value"]);

				if(!$existsMarked)
					$existsMarked = $existsMarkedCurr;
			}

			//извлекаем налоги
			$aItem = $oItem->toArray();
			$aAdjustments = $aItem["adjustments"];
			$fTax = 0;
			foreach($aAdjustments as $aAdjustment)
			{
				$oAdjustment = $aAdjustment["value"];
				if($oAdjustment->getType() == 'tax')
				{
					$fTax = $oAdjustment->getPercentage()*100;
					break;
				}
			}

			$aItems2[] = [
				"name" => $oItem->getTitle(),
				"count" => intval($oItem->getQuantity()),
				"price" => $oItem->getUnitPrice()->getNumber(),
				"vat" => $fTax,
				"marked" => $existsMarkedCurr,
				"mark" => ($existsMarkedCurr ? ($aItem["datamatrix"][0]["value"]) : "")
			];
		}

		return ["exists_marked" => $existsMarked, "items" => $aItems2];
	}

	//**************************************************************************

	/*! отправка данных чека на Пангею
		@param idOrder ид заказа
		@param aUserData ассоциативный массив данных юзера
		@param aItems массив товаров #getOrderData [items]
		@return [success => true/false, msg => сообщение]
	*/
	static public function sendToCheckout($idOrder, $aUserData, $aItems)
	{
		$aReturn = ["success" => false, "msg" => ""];
		$aContent = self::getContent($aUserData, $aItems, true);

		$aModuleSettings = self::getSettings();
		$sUUID = CPangaea::getUUID();
		$oPangaea = new CPangaea($aModuleSettings["tin"], $aModuleSettings["token"]);
		$aResponse = $oPangaea->receipt($aModuleSettings["store"], $aContent, $sUUID);
			
		$sMessage = "Заказ №$idOrder: Поставлено в очередь";

		if($aResponse["code"] == 415)
		{
			$aReturn["msg"] = "Заказ №$idOrder: Указан неверный код маркировки";
			return $aReturn;
		}
		else if($aResponse["code"] == 401 || $aResponse["code"] == 404)
		{
			$aReturn["msg"] = ("Неверные авторизационные данные, проверьте настройки");
			return $aReturn;
		}
		else if($aResponse["code"] == 402)
			$sMessage = ("Заказ №$idOrder: Данные отправлены, однако они будут обработаны после поступления оплаты");
		else if($aResponse["code"] >= 400 && $aResponse["code"] < 500)
			$sMessage = ("Заказ №$idOrder: Неизвестная ошибка, чек поставлен в очередь");

		$iStatus = ($aResponse["code"] == 201 ? 1 : 0);
			
		$query = \Drupal::database()->insert('fermarunet_checkout_tabs');
		$query->fields([
			'order' => $idOrder,
			'status' => $iStatus,
			'uuid' => $sUUID,
			'content' => json_encode($aContent, JSON_UNESCAPED_UNICODE),
			'response_txt' => $aResponse["response"],
			'response_code' => $aResponse["code"],
			'timestamp' => date("Y-m-d H:i:s"),
			'count' => 1,
		]);
		$query->execute();

		$aReturn["success"] = true;
		$aReturn["msg"] = $sMessage;
		
		return $aReturn;
	}
};
