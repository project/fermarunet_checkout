setInterval(() => {
	let aDivs = document.querySelectorAll("#checkCanv");
	
	if(aDivs.length == 0)
		return;
		
	let oDiv = aDivs[aDivs.length-1];
	if(oDiv.hasAttribute("qr"))
	{
		let oQR = new QRCode(oDiv, {text: oDiv.getAttribute("qr"), width: 128, height: 128});
		oDiv.removeAttribute("qr");
	}
},
2000);