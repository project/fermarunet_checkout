<?php

namespace Drupal\fermarunet_checkout\Form;

use Drupal\fermarunet_checkout\CPangaea;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

//##########################################################################

class SettingsForm extends ConfigFormBase 
{
  public function getFormId() 
	{
    return 'fermarunet_checkout_admin_settings';
  }

	//************************************************************************

  protected function getEditableConfigNames() 
	{
    return [
      'fermarunet_checkout.settings',
    ];
  }
 
	//************************************************************************

  public function buildForm(array $form, FormStateInterface $form_state) 
	{
    $config = $this->config('fermarunet_checkout.settings');
 
    $form['store_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ID магазина'),
      '#default_value' => $config->get('store_id'),
		];
 
    $form['company_tin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ИНН компании'),
      '#default_value' => $config->get('company_tin'),
		];

		$form['company_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Токен компании'),
      '#default_value' => $config->get('company_token'),
		];
 
    return parent::buildForm($form, $form_state);
	}
	
	//************************************************************************

	public function validateForm(array &$form, FormStateInterface $form_state)
	{
		$oPangaea = new CPangaea($form_state->getValue('company_tin'), $form_state->getValue('company_token'));
		$aRes = $oPangaea->company();
		if($aRes["code"] != 200)
			$sError = $aRes["response"];

		if(strlen($sError) == 0)
		{
			$aRes = $oPangaea->store($form_state->getValue('store_id'));
			if($aRes["code"] != 200)
				$sError = $aRes["response"];
		}
	 
		if(strlen($sError) != 0)
			$form_state->setErrorByName('title', $this->t($sError));
	}
 
  //************************************************************************

  public function submitForm(array &$form, FormStateInterface $form_state) 
	{
		$this->configFactory->getEditable('fermarunet_checkout.settings')
			->set('store_id', $form_state->getValue('store_id'))
			->set('company_tin', $form_state->getValue('company_tin'))
			->set('company_token', $form_state->getValue('company_token'))
			->save();
	
		parent::submitForm($form, $form_state);
  }
}
