<?php

namespace Drupal\fermarunet_checkout\Form;

use Drupal\fermarunet_checkout\CPangaea;
use Drupal\fermarunet_checkout\func;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

//##########################################################################

class ReturnForm extends ConfirmFormBase 
{
  public function getFormId() 
	{
    return 'fermarunet_checkout_return_form';
	}

	public function getCancelUrl() 
	{
		return null;
  }

	public function getQuestion()
	{
    return t('Отменить заказ %id?', ['%id' => $_GET["order"]]);
  }
 
	//************************************************************************

  public function buildForm(array $form, FormStateInterface $form_state) 
	{
		$idOrder = $_GET["order"];
		
		$aResult = $this->getOrderFromDB($idOrder);
		$existsReturn = $aResult["exists_return"];
		$aRes = $aResult["result"];

		if($existsReturn)
			return null;
		else if($aRes == null)
			return null;
		else
		{
			$form["notes"] = [
				'#markup' => "<p>Отметьте товары для возврата:</p>"
			];

			$aContent = json_decode($aRes["content"], true);
			$iNumber = 0;
			
			foreach($aContent["t1059"] as $aItem)
			{
				for($i=0; $i<$aItem["t1023"]; ++$i)
				{
					$sIdCheckbox = $idOrder."_".($iNumber++);
					$form[$sIdCheckbox] = [
						'#type' => 'checkbox',
						'#title' => $aItem["t1030"]
					];
				}
			}

			$form["order_edit"] = [
				'#markup' => "<p><a href='/admin/commerce/orders/$idOrder/edit'>Редактировать заказ</a></p>"
			];
		}

    return parent::buildForm($form, $form_state);
	}
	
	//************************************************************************

	public function validateForm(array &$form, FormStateInterface $form_state)
	{
		$idOrder = $_GET["order"];

		$aResult = $this->getOrderFromDB($idOrder);
		$existsReturn = $aResult["exists_return"];
		$aRes = $aResult["result"];
		
		if($existsReturn)
			return $form_state->setError($form, "У заказа $idOrder уже есть возврат");
		else if($aRes == null)
			return $form_state->setError($form, "Заказ $idOrder еще не фискализирован");

		parent::validateForm($form, $form_state);
		

		$aValues = $form_state->getValues();
		$aFormElements = [];
		foreach($aValues as $sKey => $sValue)
		{
			if(is_numeric($sValue) && preg_match("/^{$idOrder}_(\d+)$/", $sKey, $aMatch))
				$aFormElements[$aMatch[1]] = $sValue;
		}
		ksort($aFormElements);

		$aCheckData = json_decode($aRes["content"], true);
		/*file_put_contents("validateForm.txt", print_r($aCheckData, true));
		return;*/
		//Сумма электронными (в копейках)
		$s1081 = 0;

		/* проход по массиву товаров
			в одной позиции количество товара может быть > 1
			т.к. определение товара к возврату по порядковому номеру то
			внутри проход по количеству товаров в позиции 
		*/
		$aReturnItems = [];
		$iNumber = 0;
		foreach($aCheckData["t1059"] as $aItem)
		{
			for($k=0, $kl=$aItem["t1023"]; $k<$kl; ++$k)
			{
				if(array_key_exists($iNumber, $aFormElements) && $aFormElements[$iNumber])
				{
					$aReturnItem = $aItem;
					$aReturnItem["t1023"] = 1;
					$aReturnItems[] = $aReturnItem;
					$s1081 += $aReturnItem["t1079"];
				}
				++$iNumber;
			}
		}

		//exit_print_r($aReturnItems);

		$aCheckData["t1054"] = 2;
		$aCheckData["t1059"] = $aReturnItems;
		$aCheckData["t1081"] = $s1081;

		$aModuleSettings = func::getSettings();
		//file_put_contents("module_settings.txt", print_r($aModuleSettings, true));
		$sUUID = CPangaea::getUUID();
		$oPangaea = new CPangaea($aModuleSettings["tin"], $aModuleSettings["token"]);
		$aResponse = $oPangaea->receipt($aModuleSettings["store"], $aCheckData, $sUUID);
			
		if($aResponse["code"] == 415)
		{
			$form_state->setError($form, "Заказ №$idOrder: Указан неверный код маркировки");
			return;
		}
		else if($aResponse["code"] == 401 || $aResponse["code"] == 404)
		{
			$form = ["error" => ["#markup" => "Проверьте <a href='/admin/commerce/fiscalization/settings'>настройки</a>"]] + $form;
			$form_state->setError($form, "Неверные авторизационные данные");
			return;
		}
		/*else if($aResponse["code"] == 402)
			$form_state->setError($form, "Заказ №$idOrder: Данные отправлены, однако они будут обработаны после поступления оплаты");
		else if($aResponse["code"] >= 400 && $aResponse["code"] < 500)
			$form_state->setError($form, "Заказ №$idOrder: Неизвестная ошибка, чек поставлен в очередь");*/

		$iStatus = ($aResponse["code"] == 201 ? 2 : 0);
			
		$query = \Drupal::database()->insert('fermarunet_checkout_tabs');
		$query->fields([
			'order' => $idOrder,
			'status' => $iStatus,
			'uuid' => $sUUID,
			'content' => json_encode($aCheckData, JSON_UNESCAPED_UNICODE),
			'response_txt' => $aResponse["response"],
			'response_code' => $aResponse["code"],
			'timestamp' => date("Y-m-d H:i:s"),
			'count' => 1,
		]);
		$query->execute();
	}
 
  //************************************************************************

  public function submitForm(array &$form, FormStateInterface $form_state)
	{
		$form_state->setRedirectUrl(Url::fromUri("internal:/admin/commerce/orders/".$_GET["order"]));
	}
	
	//########################################################################
	//PROTECTED
	//########################################################################

	protected function getOrderFromDB($idOrder)
	{
		$sSQL = "SELECT * FROM `fermarunet_checkout_tabs` WHERE `order`=$idOrder";
    $sQuery = \Drupal::database()->query($sSQL);
		$aResults = $sQuery->fetchAll();
		
		$existsReturn = false;
		$aRes = null;

		foreach($aResults as $oResult)
		{
			$aResult = get_object_vars($oResult);

			if($aResult["status"] == 1)
				$aRes = $aResult;

			$aContent = json_decode($aResult["content"], true);
	
			if($aResult["status"] == 2 || $aContent["t1054"] == 2)
				$existsReturn = true;
		}

		return [
			"exists_return" => $existsReturn,
			"result" => $aRes
		];
	}
}
