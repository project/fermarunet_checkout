<?php

namespace Drupal\fermarunet_checkout;

class CPangaea
{
	const API_URL = "https://api.kassavoblake.com/v1";

	static public $m_aTaxation = [
		0x01 => "Общая",
		0x02 => "Упрощенная доход, УСН доход",
		0x04 => "Упрощенная доход минус расход, УСН доход - расход",
		0x08 => "Единый налог на вмененный доход, ЕНВД",
		0x10 => "Единый сельскохозяйственный налог, ЕСН",
		0x20 => "Патентная система налогообложения, Патент",
	];

	//########################################################################

	static public function getTaxationText($iNumber)
	{
		return self::$m_aTaxation[$iNumber];
	}

	//************************************************************************

	static public function getUUID()
	{
		//https://stackoverflow.com/questions/2040240/php-function-to-generate-v4-uuid
		$sUUID = sprintf(
			'%04x%04x%04x%04x%04x%04x%04x%04x',
			random_int(0, 0xffff), random_int(0, 0xffff),
			random_int(0, 0xffff),
			random_int(0, 0x0fff) | 0x4000,
			random_int(0, 0x3fff) | 0x8000,
			random_int(0, 0xffff), random_int(0, 0xffff), random_int(0, 0xffff)
		);

		return $sUUID;
	}

	//########################################################################

	public function __construct($sTin, $sToken)
	{
		$this->m_sTin = $sTin;
		$this->m_sToken = $sToken;
		$this->m_sAuthKey = "$sTin:$sToken";
		$this->m_hCurl = curl_init();
	}

	//########################################################################

	public function receipt($idStore, $aContent, $sUUID)
	{
		$this->reset();
		$aHeaders = [
			"Idempotency-Key: $sUUID",
			"Authorization: ".$this->m_sAuthKey,
			"Content-type: application/json; charset=utf-8",
		];
		curl_setopt($this->m_hCurl, CURLOPT_URL, self::API_URL."/receipt/$idStore");
		curl_setopt($this->m_hCurl, CURLOPT_HTTPHEADER, $aHeaders);
		curl_setopt($this->m_hCurl, CURLOPT_POST, TRUE);
		curl_setopt($this->m_hCurl, CURLOPT_POSTFIELDS, json_encode($aContent, JSON_UNESCAPED_UNICODE));

		file_put_contents("receipt.txt", print_r(curl_getinfo($this->m_hCurl), true));

		return $this->exec();
	}

	//************************************************************************

	public function company()
	{
		$this->reset();
		curl_setopt($this->m_hCurl, CURLOPT_URL, self::API_URL."/company");
		curl_setopt($this->m_hCurl, CURLOPT_HTTPHEADER, ["Authorization: ".$this->m_sAuthKey]);

		return $this->exec();
	}

	//************************************************************************

	public function store($idStore)
	{
		$this->reset();
		curl_setopt($this->m_hCurl, CURLOPT_URL, self::API_URL."/company/c_groups/$idStore");
		curl_setopt($this->m_hCurl, CURLOPT_HTTPHEADER, ["Authorization: ".$this->m_sAuthKey]);

		return $this->exec();
	}

	//########################################################################
	//PROTECTED
	//########################################################################

	protected $m_sTin;
	protected $m_sToken;
	protected $m_sAuthKey;
	protected $m_aHeaders = [];
	protected $m_hCurl = null;

	//########################################################################

	protected function reset()
	{
		curl_reset($this->m_hCurl);
		curl_setopt($this->m_hCurl, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($this->m_hCurl, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($this->m_hCurl, CURLOPT_RETURNTRANSFER, 1);
	}

	//************************************************************************

	protected function exec()
	{
		$sResponse = curl_exec($this->m_hCurl);
		$sCode = curl_getinfo($this->m_hCurl, CURLINFO_RESPONSE_CODE);

		$aData = null;
		if($sCode == 200 || $sCode == 201)
			$aData = json_decode($sResponse, true);

		return [
			"code" => $sCode,
			"response" => $sResponse,
			"data" => $aData
		];
	}
};
